/*
  Warnings:

  - Added the required column `creator` to the `Messages` table without a default value. This is not possible if the table is not empty.
  - Added the required column `importedId` to the `Messages` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Messages" ADD COLUMN     "creator" TEXT NOT NULL,
ADD COLUMN     "importedId" TEXT NOT NULL;

-- CreateTable
CREATE TABLE "Imported" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "userId" TEXT NOT NULL,

    CONSTRAINT "Imported_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Messages" ADD CONSTRAINT "Messages_importedId_fkey" FOREIGN KEY ("importedId") REFERENCES "Imported"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Imported" ADD CONSTRAINT "Imported_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
