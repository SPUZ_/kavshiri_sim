-- AlterTable
ALTER TABLE "Contact" ADD COLUMN     "updatedAt" TIMESTAMP(3);

-- AlterTable
ALTER TABLE "Number" ADD COLUMN     "updatedAt" TIMESTAMP(3);
