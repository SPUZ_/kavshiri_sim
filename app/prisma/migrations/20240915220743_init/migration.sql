-- DropForeignKey
ALTER TABLE "Messages" DROP CONSTRAINT "Messages_importedId_fkey";

-- AlterTable
ALTER TABLE "Messages" ALTER COLUMN "importedId" DROP NOT NULL,
ALTER COLUMN "creatorApp" DROP NOT NULL,
ALTER COLUMN "creator" DROP NOT NULL;

-- AddForeignKey
ALTER TABLE "Messages" ADD CONSTRAINT "Messages_importedId_fkey" FOREIGN KEY ("importedId") REFERENCES "Imported"("id") ON DELETE SET NULL ON UPDATE CASCADE;
