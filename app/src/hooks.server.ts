import { type Handle, redirect } from '@sveltejs/kit';
import { CookieName } from '$lib/server/cfgs';
import { checkSession } from '$lib/server/actions/users/sessions/check';
import { getUserById } from '$lib/server/actions/users/getUserById';
const unProtectedRoutes = ['/login'];

export const handle: Handle = async ({ event, resolve }) => {
	const sessionId = event.cookies.get(CookieName);
	if (unProtectedRoutes.includes(event.url.pathname)) {
		return resolve(event);
	}

	if (sessionId === undefined) {
		throw redirect(303, '/login');
	}

	const userId = await checkSession(sessionId);

	if (userId) {
		const currentUser = await getUserById(userId);
		if (currentUser) {
			event.locals.user = {
				id: currentUser.id,
				username: currentUser.username,
				email: currentUser.email
			};
		} else {
			if (!unProtectedRoutes.includes(event.url.pathname)) {
				throw redirect(303, '/login');
			}
		}
	} else {
		if (!unProtectedRoutes.includes(event.url.pathname)) {
			throw redirect(303, '/login');
		}
	}

	return resolve(event);
};
