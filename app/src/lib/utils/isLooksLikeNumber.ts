/**
 * Checks if the given string looks like a number.
 * @param input - The string to check.
 * @returns True if the string looks like a number, otherwise false.
 */
export function isLooksLikeNumber(input: string): boolean {
	const numberRegex = /^\d{9,}$/;
	return numberRegex.test(input.trim());
}
