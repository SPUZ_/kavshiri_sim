/**
 * Shortens a string to a specified maximum length, appending "..." if the string is too long.
 * @param input - The input string to be shortened.
 * @param maxLength - The maximum length of the output string, including the "..." suffix.
 * @returns The shortened string with "..." appended if necessary.
 */
export function shortString(input: string, maxLength: number): string {
	const ellipsis = '...';
	const ellipsisLength = ellipsis.length;

	if (input.length <= maxLength) {
		return input;
	}

	// Calculate the length of the string before adding "..."
	const maxContentLength = Math.max(maxLength - ellipsisLength, 0);

	return input.slice(0, maxContentLength) + ellipsis;
}
