export function generateRandomString(length: number) {
	return Math.random().toString(36).slice(-length);
}

/**
 * Created string without weird characters
 */
export const generateRandomStringProtected = (length: number) =>
	[...Array(length)].map(() => Math.random().toString(36)[2]).join('');
