import { parsePhoneNumber } from 'libphonenumber-js';
import { isLooksLikeNumber } from '$lib/utils/isLooksLikeNumber';
/**
 * Checks if the given string looks like a number.
 * @param input - The string to check.
 * @returns number.
 */
export function formatNumber(input: string) {
	if (isLooksLikeNumber(input)) {
		try {
			const phoneNumber = parsePhoneNumber(input.trim());
			return phoneNumber.formatInternational();
		} catch {
			try {
				const phoneNumber = parsePhoneNumber('+' + input.trim());
				return phoneNumber.formatInternational();
			} catch {
				return input;
			}
		}
	} else {
		return input;
	}
}
