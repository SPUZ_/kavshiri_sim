/**
 * Copies the given text to the clipboard.
 * @param text - The text to be copied to the clipboard.
 */
export default function copyToClipboard(text: string): void {
	// Modern approach using the Clipboard API
	if (navigator.clipboard) {
		navigator.clipboard
			.writeText(text)
			.then(() => {
				console.log('Text copied to clipboard');
			})
			.catch((err) => {
				console.error('Failed to copy: ', err);
			});
	} else {
		// Fallback for older browsers
		const textarea = document.createElement('textarea');
		textarea.value = text;
		document.body.appendChild(textarea);
		textarea.select();
		document.execCommand('copy');
		document.body.removeChild(textarea);
		console.log('Text copied to clipboard (fallback)');
	}
}
