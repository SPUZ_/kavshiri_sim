export function formatDateMessage(inputDate: Date) {
	const now = new Date();

	// Check if the given date is today
	const isToday = now.toDateString() === inputDate.toDateString();

	// Check if the given date is within the same year
	const isSameYear = now.getFullYear() === inputDate.getFullYear();

	if (isToday) {
		// Format as "HH:MM"
		return inputDate.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', hour12: false });
	} else if (isSameYear) {
		// Format as "Month HH:MM"
		const month = inputDate.toLocaleString('default', { month: 'long' });
		const time = inputDate.toLocaleTimeString([], {
			hour: '2-digit',
			minute: '2-digit',
			hour12: false
		});
		return `${month} ${time}`;
	} else {
		return 'A long ago';
	}
}
