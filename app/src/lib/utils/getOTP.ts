export default function getOTP(text: string) {
	const regex = /\b\d{4,12}\b/;
	const match = text.match(regex);
	if (match) {
		return match[0];
	}
	return false;
}
