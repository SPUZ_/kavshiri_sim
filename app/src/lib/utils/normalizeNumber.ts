export function normalizeNumber(number: string) {
	const cleanedNumber = number.replace(/[^0-9]/g, '');
	return `+${cleanedNumber.length === 10 ? `1${cleanedNumber}` : cleanedNumber}`;
}
