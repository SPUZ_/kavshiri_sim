const emailReg =
	// eslint-disable-next-line
	/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export function validateEmail(email: string) {
	if (!email) {
		return 'Email is required';
	}

	if (!emailReg.test(email)) {
		return 'Invalid email address';
	}
}

export function validatePassword(pass: string) {
	if (!pass) {
		return 'Pass is required';
	}
}
