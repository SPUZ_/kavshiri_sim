import Ajv from 'ajv';
import type { JTDDataType } from 'ajv/dist/jtd';

const ajv = new Ajv();

const schemaImportJsonMessageReceived = {
	type: 'object',
	properties: {
		_id: { type: 'string' },
		thread_id: { type: 'string' },
		address: { type: 'string' },
		date: { type: 'string', pattern: '\\d+' },
		date_sent: { type: 'string', pattern: '\\d+' },
		protocol: { type: 'string' },
		read: { type: 'string' },
		status: { type: 'string' },
		type: { type: 'string' },
		reply_path_present: { type: 'string' },
		body: { type: 'string' },
		service_center: { type: 'string' },
		locked: { type: 'string' },
		sub_id: { type: 'string' },
		error_code: { type: 'string' },
		creator: { type: 'string' },
		seen: { type: 'string' },
		priority: { type: 'string' }
	},
	required: [
		'_id',
		'thread_id',
		'address',
		'date',
		'date_sent',
		'protocol',
		'read',
		'status',
		'type',
		'reply_path_present',
		'body',
		'service_center',
		'locked',
		'sub_id',
		'error_code',
		'creator',
		'seen',
		'priority'
	],
	additionalProperties: false
};

const schemaImportJsonMessageSent = {
	type: 'object',
	properties: {
		_id: {
			type: 'string'
		},
		thread_id: {
			type: 'string'
		},
		address: {
			type: 'string'
		},
		date: {
			type: 'string'
		},
		date_sent: {
			type: 'string'
		},
		read: {
			type: 'string'
		},
		status: {
			type: 'string'
		},
		type: {
			type: 'string'
		},
		body: {
			type: 'string'
		},
		locked: {
			type: 'string'
		},
		sub_id: {
			type: 'string'
		},
		error_code: {
			type: 'string'
		},
		creator: {
			type: 'string'
		},
		seen: {
			type: 'string'
		},
		priority: {
			type: 'string'
		}
	},
	required: [
		'_id',
		'thread_id',
		'address',
		'date',
		'read',
		'type',
		'body',
		'locked',
		'creator',
		'seen'
	],
	additionalProperties: false
};

export type ImportJsonMessageReceivedTS = JTDDataType<typeof schemaImportJsonMessageReceived>;
export type ImportJsonMessageSentTS = JTDDataType<typeof schemaImportJsonMessageSent>;

export const validateImportReceived = ajv.compile<ImportJsonMessageReceivedTS>(
	schemaImportJsonMessageReceived
);
export const validateImportSent = ajv.compile<ImportJsonMessageSentTS>(schemaImportJsonMessageSent);
