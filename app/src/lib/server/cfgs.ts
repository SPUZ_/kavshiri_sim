export const CookieSecure = process.env.NODE_ENV === 'production';
export const CookieMaxAge = 60 * 60 * 24 * 30;
export const CookieName = 'kavshiriSimAuth';
export const AllowedToRegister = 1;
