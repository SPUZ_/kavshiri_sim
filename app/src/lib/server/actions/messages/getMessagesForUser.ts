import prisma from '$lib/server/database/prisma';
import type { Messages } from '@prisma/client';

type MinimalContact = {
	id: string;
	name: string;
};

export type GetMessagesForUserResult = {
	contact: MinimalContact | null;
	messages: Messages[];
};

export const getMessagesForUser = async (
	userId: string,
	number_raw?: string,
	contact_id?: string
): Promise<GetMessagesForUserResult> => {
	// Try to find a contact associated with the given user and either the contact ID
	const contact = await prisma.contact.findFirst({
		where: {
			userId,
			id: contact_id
		},
		include: {
			numbers: {
				include: {
					Messages: true
				}
			}
		}
	});

	// If no contact is found, fetch messages directly associated with the given number
	if (!contact) {
		const messages = await prisma.messages.findMany({
			where: {
				userId,
				numberRaw: number_raw
			}
		});
		return {
			contact: null,
			messages
		};
	}

	// Select specific contact fields to return (e.g., contact.id, contact.name)
	const minimalContact = {
		id: contact.id, // Keep the contact ID
		name: contact.name // Include the contact name or other essential details
	};

	// Return the found contact and all associated messages
	const messages = contact.numbers.flatMap((numberEntry) => {
		return { ...numberEntry.Messages };
	});

	return {
		contact: minimalContact,
		messages
	};
};
