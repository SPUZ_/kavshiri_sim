import prisma from '$lib/server/database/prisma';
import type { Prisma } from '@prisma/client';

// Define the payload type based on the select statement
export type LatestMessage = Prisma.MessagesGetPayload<{
	select: {
		content: true;
		numberRaw: true;
		createdAt: true;
		Number: {
			select: {
				number: true;
				Contact: {
					select: {
						name: true;
						id: true;
					};
				};
			};
		};
	};
}>;

export const getListMessagesForUser = async (userId: string) => {
	// Step 1: Use a raw SQL query to get the latest message IDs per group
	const latestMessagesData = await prisma.$queryRaw<{ id: string }[]>`
    SELECT id FROM (
      SELECT
        id,
        ROW_NUMBER() OVER (
          PARTITION BY "numberId", "numberRaw"
          ORDER BY "createdAt" DESC
        ) as row_num
      FROM "Messages"
      WHERE "userId" = ${userId}
    ) sub
    WHERE row_num = 1
  `;

	// Step 2: Extract IDs
	const latestMessageIds = latestMessagesData.map((row) => row.id);

	// Step 3: Fetch messages with relations
	const latestMessages: LatestMessage[] = await prisma.messages.findMany({
		where: { id: { in: latestMessageIds } },
		select: {
			content: true,
			numberRaw: true,
			createdAt: true,
			Number: {
				select: {
					number: true,
					Contact: {
						select: {
							name: true,
							id: true
						}
					}
				}
			}
		},
		orderBy: {
			createdAt: 'desc'
		}
	});

	return latestMessages;
};
