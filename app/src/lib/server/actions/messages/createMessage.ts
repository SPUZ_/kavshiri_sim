import prisma from '$lib/server/database/prisma';

export const createMessage = async (
	userId: string,
	number: string,
	content: string,
	creator?: string,
	creatorApp?: string,
	importedId?: string,
	createdAt?: Date,
	isSent = false
) => {
	// Getting number
	const numberIdDB = await prisma.number.findFirst({
		where: {
			Contact: {
				userId: userId
			},
			number: number
		},
		select: {
			id: true
		}
	});

	let numberRaw = null;
	let numberId = null;
	if (numberIdDB) {
		numberId = numberIdDB.id;
	} else {
		numberRaw = number;
	}

	// Creating message
	return prisma.messages.create({
		data: {
			userId: userId,
			numberRaw: numberRaw,
			numberId: numberId,
			creator: creator,
			creatorApp: creatorApp,
			importedId: importedId,
			content: content,
			createdAt: createdAt,
			isSent: isSent
		}
	});
};
