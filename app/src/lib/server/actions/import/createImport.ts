import prisma from '$lib/server/database/prisma';

export const createImport = async (userId: string) => {
	return prisma.imported.create({
		data: { userId: userId }
	});
};
