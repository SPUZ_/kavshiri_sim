import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

/**
 * Removes an Imported record by userId and importId,
 * and deletes all associated Messages.
 *
 * @param userId - The ID of the user.
 * @param importId - The ID of the import to delete.
 */
export async function removeImportAndMessages(userId: string, importId: string): Promise<void> {
	try {
		await prisma.$transaction(async (tx) => {
			// Verify that the Imported record exists and belongs to the user
			const imported = await tx.imported.findUnique({
				where: {
					id: importId,
					userId: userId
				},
				select: {
					userId: true
				}
			});

			if (!imported) {
				throw new Error('Imported record not found.');
			}

			// Delete all Messages associated with this Imported record
			await tx.messages.deleteMany({
				where: {
					importedId: importId
				}
			});

			// Delete the Imported record
			await tx.imported.delete({
				where: {
					id: importId
				}
			});
		});
	} catch (error) {
		console.error(`Error deleting import and messages: ${(error as Error).message}`);
		throw error; // Re-throw the error after logging
	}
}
