import prisma from '$lib/server/database/prisma';

export const getImportForUser = async (userId: string) => {
	return prisma.imported.findMany({
		where: { userId: userId },
		include: {
			_count: {
				select: { Messages: true }
			}
		}
	});
};
