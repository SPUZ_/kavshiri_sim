import prisma from '$lib/server/database/prisma';
import bcrypt from 'bcrypt';

export const registerUser = async (username: string, email: string, password: string) => {
	const newPassword = await bcrypt.hash(password, 10);

	const user = await prisma.user.create({
		data: {
			username,
			email,
			password: newPassword
		}
	});

	return user;
};
