import { redis } from '$lib/server/database/redis';
import { CookieMaxAge } from '$lib/server/cfgs';

export async function createNewSession(username: string) {
	const token = crypto.randomUUID();
	await redis.set(`session:${token}`, username, 'EX', CookieMaxAge);
	return token;
}
