import { redis } from '$lib/server/database/redis';

export async function checkSession(token: string) {
	const username = await redis.get(`session:${token}`);
	return username;
}
