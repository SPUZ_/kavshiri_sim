import prisma from '$lib/server/database/prisma';

export const getNumbersOfUsers = async () => {
	const users = await prisma.user.count();
	return users;
};
