import prisma from '$lib/server/database/prisma';

export const getUserById = async (id: string) => {
	const user = await prisma.user.findFirst({ where: { id } });
	return user;
};
