import prisma from '$lib/server/database/prisma';

export const removeNumber = async (numberToRemove: string) => {
	const numberToDelete = await prisma.number.findUnique({
		where: { id: numberToRemove },
		select: { number: true }
	});

	if (!numberToDelete) {
		throw new Error('Number not found');
	}

	await prisma.messages.updateMany({
		where: { numberId: numberToRemove },
		data: {
			numberId: null,
			numberRaw: numberToDelete.number
		}
	});

	return prisma.number.delete({ where: { id: numberToRemove } });
};
