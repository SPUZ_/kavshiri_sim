import prisma from '$lib/server/database/prisma';

export const createNumber = async (
	contactId: string,
	number: string,
	weirdness: boolean = false
) => {
	const newNumber = await prisma.number.create({
		data: { number: number, weirdType: weirdness, contactId: contactId }
	});

	await prisma.messages.updateMany({
		where: { numberRaw: number },
		data: {
			numberId: newNumber.id,
			numberRaw: null
		}
	});

	return newNumber;
};
