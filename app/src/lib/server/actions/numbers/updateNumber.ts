import prisma from '$lib/server/database/prisma';

export const updateNumber = async (numberId: string, number: string) => {
	console.log('here');
	const numberToUpdate = await prisma.number.findUnique({
		where: { id: numberId },
		select: { number: true }
	});

	if (!numberToUpdate) {
		throw new Error('Number not found');
	}

	await prisma.$transaction(async (tx) => {
		await tx.messages.updateMany({
			where: { numberId },
			data: {
				numberId: null,
				numberRaw: numberToUpdate.number
			}
		});

		await tx.messages.updateMany({
			where: { numberRaw: number },
			data: {
				numberId: numberId,
				numberRaw: null
			}
		});
	});
	return prisma.number.update({
		where: { id: numberId },
		data: { number: number }
	});
};
