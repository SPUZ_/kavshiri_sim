import prisma from '$lib/server/database/prisma';

export const updateContact = async (
	userId: string,
	contactId: string,
	updates: {
		name?: string;
		description?: string;
	}
) => {
	return prisma.contact.update({
		where: { id: contactId, userId: userId },
		data: updates
	});
};
