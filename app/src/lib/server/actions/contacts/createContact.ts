import prisma from '$lib/server/database/prisma';

export const createContact = async (userId: string, name: string, description: string) => {
	return prisma.contact.create({
		data: { name: name, description: description, userId: userId }
	});
};
