import prisma from '$lib/server/database/prisma';

export const getContactWithNumbersForUser = async (contactId: string, userId: string) => {
	return prisma.contact.findFirst({
		where: { id: contactId, userId: userId },
		include: { numbers: true }
	});
};
