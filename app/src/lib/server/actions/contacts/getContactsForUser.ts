import prisma from '$lib/server/database/prisma';

export const getContactsForUser = async (userId: string) => {
	const contacts = await prisma.contact.findMany({
		where: { userId: userId },
		include: {
			numbers: true
		}
	});
	return contacts;
};
