import { HTMLAttributes } from 'svelte/elements';

declare module 'svelte/elements' {
	export interface SvelteHTMLElements {
		ButtonA: HTMLAttributes<HTMLButtonElement> & {
			formaction?: string;
		};
	}
}
