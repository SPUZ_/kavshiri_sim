import type { Actions, PageServerLoad } from './$types';
import { validateEmail, validatePassword } from '$lib/auth/validators/login';
import { fail, redirect } from '@sveltejs/kit';
import bcrypt from 'bcrypt';
import { AllowedToRegister, CookieMaxAge, CookieName, CookieSecure } from '$lib/server/cfgs';
import { createNewSession } from '$lib/server/actions/users/sessions/createNew';
import { getNumbersOfUsers } from '$lib/server/actions/users/getNumbersOfUsers';
import { registerUser } from '$lib/server/actions/users/registerUser';
import { getUserByEmail } from '$lib/server/actions/users/getUserByEmail';

export const load: PageServerLoad = async () => {
	const numbersOfUsers = await getNumbersOfUsers();
	if (numbersOfUsers >= AllowedToRegister) {
		return { isAllowedToRegister: false };
	}
	return { isAllowedToRegister: true };
};

export const actions = {
	login: async ({ cookies, request }) => {
		const data = await request.formData();
		const email = data.get('email')?.toString();
		const password = data.get('password')?.toString();

		if (!email || !password) {
			return fail(400, { missing: true });
		}

		if (validateEmail(email) && validatePassword(password)) {
			return fail(400, { invalid: true });
		}

		const user = await getUserByEmail(email);

		if (!user) {
			return fail(400, { missing: true });
		}

		const passwordMatch = await bcrypt.compare(password, user.password);

		if (!passwordMatch) {
			return fail(400, { invalid: true });
		}

		const session = await createNewSession(user.id);

		cookies.set(CookieName, session, {
			path: '/',
			httpOnly: true,
			sameSite: 'strict',
			secure: CookieSecure,
			maxAge: CookieMaxAge
		});

		return redirect(303, '/');
	},
	register: async ({ cookies, request }) => {
		const data = await request.formData();

		const email = data.get('email')?.toString();
		const password = data.get('password')?.toString();
		const username = data.get('username')?.toString();

		if (!email || !password || !username) {
			return fail(400, { missing: true });
		}

		if (validateEmail(email) && validatePassword(password)) {
			return fail(400, { invalid: true });
		}

		const user = await registerUser(username, email, password);
		if (!user) {
			return fail(400, { missing: true });
		}

		const session = await createNewSession(user.id);

		cookies.set(CookieName, session, {
			path: '/',
			httpOnly: true,
			sameSite: 'strict',
			secure: CookieSecure,
			maxAge: CookieMaxAge
		});

		return { success: true };
	}
} satisfies Actions;
