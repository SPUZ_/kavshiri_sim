import type { PageServerLoad } from './$types';
import { redirect } from '@sveltejs/kit';
import { getListMessagesForUser } from '$lib/server/actions/messages/getListMessagesForUser';
import { getMessagesForUser } from '$lib/server/actions/messages/getMessagesForUser';
import { isLooksLikeNumber } from '$lib/utils/isLooksLikeNumber';
import { getContactsForUser } from '$lib/server/actions/contacts/getContactsForUser';

export const load: PageServerLoad = async ({ locals, url }) => {
	// Access user from locals
	const user = locals.user;

	if (!user) {
		throw redirect(303, '/login');
	}

	let selectedChat = url.searchParams.get('selectedChat')?.trim() || '';

	const messages = await getListMessagesForUser(user.id);

	let selectedChatData = null;
	if (selectedChat) {
		// if selectedChat looks like number then add '+'
		// Check if selectedChat looks like a number (digits only)
		if (isLooksLikeNumber(selectedChat)) {
			selectedChat = `+${selectedChat}`;
		}
		selectedChatData = await getMessagesForUser(user.id, selectedChat, selectedChat);
	}
	const contacts = await getContactsForUser(user.id);

	return {
		messages,
		selectedChat,
		selectedChatData,
		contacts
	};
};
