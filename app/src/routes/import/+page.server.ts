import type { Actions, PageServerLoad } from './$types';
import { redirect } from '@sveltejs/kit';
import { getImportForUser } from '$lib/server/actions/import/getImportForUser';
import { removeImportAndMessages } from '$lib/server/actions/import/removeImportAndMessages';

export const load: PageServerLoad = async ({ locals }) => {
	// Access user from locals
	const user = locals.user;

	if (!user) {
		throw redirect(303, '/login');
	}
	const response = await getImportForUser(user.id);

	return { imports: response };
};

export const actions: Actions = {
	default: async ({ request, locals }) => {
		const user = locals.user;

		if (!user) {
			throw redirect(303, '/login');
		}

		const data = await request.formData();
		const action = data.get('action');
		const importId = data.get('importId') as string;

		if (action === 'delete' && importId) {
			// Verify that the import belongs to the user
			try {
				await removeImportAndMessages(user.id, importId);
			} catch (e) {
				console.error(e);
				return { error: 'Failed to remove import or unauthorized action.' };
			}

			return { success: 'Import removed successfully.' };
		}

		return { error: 'Invalid action or missing import ID.' };
	}
};
