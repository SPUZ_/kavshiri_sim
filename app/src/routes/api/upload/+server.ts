import { json, error, redirect } from '@sveltejs/kit';
import type { RequestHandler } from '@sveltejs/kit';
import { createImport } from '$lib/server/actions/import/createImport';
import { createMessage } from '$lib/server/actions/messages/createMessage';
import {
	type ImportJsonMessageReceivedTS,
	validateImportReceived,
	validateImportSent
} from '$lib/models/importJson';

function parseNDJSON(ndjsonString: string) {
	return ndjsonString
		.trim() // Remove any leading or trailing whitespace
		.split('\n') // Split the string by newlines
		.map((line) => {
			try {
				return JSON.parse(line); // Parse each line as JSON
			} catch (parseError) {
				console.error('Error parsing JSON:', parseError, line);
				return null; // or handle errors as needed
			}
		})
		.filter((item) => item !== null); // Filter out any null values from parsing errors
}

// Function to handle file upload
export const POST: RequestHandler = async ({ request, locals }) => {
	// Access user from locals
	const user = locals.user;

	if (!user) {
		throw redirect(303, '/login');
	}

	try {
		// Convert the request to FormData
		const formData = await request.formData();
		const file = formData.get('file') as File;

		if (!file || file.type !== 'application/x-ndjson') {
			return error(400, 'Please upload a valid NDJSON file.');
		}

		// Read the file data from the FormData
		const fileContents = await file.text(); // Read file as text

		// Parse the NDJSON data
		try {
			const jsonData = parseNDJSON(fileContents);
			const importDb = await createImport(user.id);
			jsonData.map(async (message: ImportJsonMessageReceivedTS) => {
				const isSent = validateImportSent(message);
				const isReceived = validateImportReceived(message);

				if (isSent || isReceived) {
					await createMessage(
						user.id,
						<string>message.address,
						<string>message.body,
						undefined,
						<string>message.creator,
						importDb.id,
						new Date(Number(message.date)),
						isSent
					);
				}
			});
		} catch (parseError) {
			console.error('Error parsing NDJSON:', parseError);
			return error(400, 'Invalid NDJSON file.');
		}

		return json({ success: true }); // Return a success response
	} catch (requestError) {
		console.error('Error processing the file:', requestError);
		return error(500, 'Error processing the file.');
	}
};
