import type { PageServerLoad, Actions } from './$types';
import { redirect } from '@sveltejs/kit';
import { getContactWithNumbersForUser } from '$lib/server/actions/contacts/getContactWithNumbersForUser';
import { removeNumber } from '$lib/server/actions/numbers/removeNumber';
import { updateNumber } from '$lib/server/actions/numbers/updateNumber';
import { createNumber } from '$lib/server/actions/numbers/createNumber';
import { parsePhoneNumber } from 'libphonenumber-js/min';
import { createContact } from '$lib/server/actions/contacts/createContact';
import { updateContact } from '$lib/server/actions/contacts/updateContact';

export const load: PageServerLoad = async ({ locals, params }) => {
	const user = locals.user;

	if (!user) {
		throw redirect(303, '/login');
	}

	const contactId = params.id;
	if (contactId === 'add') {
		return;
	}
	const response = await getContactWithNumbersForUser(contactId, user.id);

	return { contact: response };
};

export const actions: Actions = {
	default: async ({ request, locals, params }) => {
		const user = locals.user;
		if (!user) {
			throw redirect(303, '/login');
		}

		const contactId = params.id;
		const data = await request.formData();
		const formEntries = Object.fromEntries(data.entries());

		const name = formEntries['name'].toString() || '';
		const description = formEntries['description'].toString() || '';
		const numbers = Object.keys(formEntries);

		if (contactId === 'add') {
			numbers
				.filter((key) => key.startsWith('number-'))
				.map((key) => {
					try {
						const phoneNumber = parsePhoneNumber(formEntries[key].toString());
						return phoneNumber.number;
					} catch {
						return null;
					}
				})
				.filter(Boolean);

			if (name) {
				const newContact = await createContact(user.id, name, description);

				if (newContact && numbers.length > 0) {
					for (const number of numbers) {
						await createNumber(newContact.id, number?.toString() || '');
					}
				}

				return redirect(303, `/contacts/${newContact.id}`);
			}
			return;
		}

		const contact = await getContactWithNumbersForUser(contactId, user.id);
		if (!contact) {
			return;
		}

		if (name !== contact.name || description !== contact.description) {
			await updateContact(user.id, contact.id, { name, description });
		}

		const contactIds = contact.numbers.map((r) => r.id);
		const contactNumbers = contact.numbers.map((r) => r.number);
		for (const key of numbers) {
			if (key.startsWith('number-')) {
				// Checking if user owns this number
				const match = key.match(/^number-([^-]+)(?:-|$)/);
				if (match) {
					const idPart = match[1];
					if (contactIds.includes(idPart) || key.includes('new')) {
						let number: string = '';
						try {
							if (typeof formEntries[key] === 'string') {
								const phoneNumber = parsePhoneNumber(formEntries[key]);
								number = phoneNumber.number;
							} else {
								number = '';
							}
						} catch (e) {
							if (e instanceof Error) {
								number = '';
							}
						}

						const exist = contactNumbers.includes(number);

						if (key.includes('deleted')) {
							await removeNumber(idPart);
						} else if (key.includes('edited')) {
							if (number && !exist) {
								await updateNumber(idPart, number);
							}
						} else if (key.includes('new')) {
							if (number && !exist) {
								await createNumber(contactId, number);
							}
						}
					}
				}
			}
		}
		return;
	}
};
