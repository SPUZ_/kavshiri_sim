import { getContactsForUser } from '$lib/server/actions/contacts/getContactsForUser';
import type { PageServerLoad } from './$types';
import { redirect } from '@sveltejs/kit';

export const load: PageServerLoad = async ({ locals }) => {
	// Access user from locals
	const user = locals.user;

	if (!user) {
		throw redirect(303, '/login');
	}
	const response = await getContactsForUser(user.id);

	return { contacts: response };
};
